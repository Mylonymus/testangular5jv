import { UserService } from './../../user.service';
import { Component, Inject, Input, Output, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { User } from '../../User';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
 
export class IndexComponent implements OnInit {

    users: any;
    public registerUser: boolean = true;
  constructor(
    private http: HttpClient,
    private service: UserService
     ) {}

  ngOnInit() {
    this.getUsers();
    
  }

  getUsers() {
    
    const user = JSON.parse(localStorage.getItem('registerUser'));
    if(localStorage.getItem('registerUser') != undefined){
      this.registerUser = true;
      this.service.getUsers(user.idToken).then(res => {
          
        localStorage.setItem('users', JSON.stringify(res));
        this.users = res;
      });
    }else{
      this.registerUser = false;
      this.users = JSON.parse(localStorage.getItem('users'));
    }
    
  }
  
  deleteUser(id) {
    const self = this;
    this.service.deleteUser(id).then(res => {
      console.log('Deleted');
      self.getUsers();
    });
  }
}
