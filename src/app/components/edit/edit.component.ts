import { Component, Input, Output, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../../user.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms'; 
import { IndexComponent } from '../index/index.component';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  user: any;
  angForm: FormGroup;

  title = 'Edit User';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: UserService,
    private fb: FormBuilder,
    @Inject(IndexComponent) parent: IndexComponent
  ) {
    
   }

  createForm() {

    this.angForm = this.fb.group({
      first_name: ['', Validators.required ],
      last_name: ['', Validators.required ],
      iban: ['', Validators.required ]
   });
  }

  updateUser(name, lastname, iban) {
    const self = this;
    this.route.params.subscribe(params => {

      this.service.updateUser(name, lastname, iban, params['id']).then(function() {
        self.router.navigate(['index']);
      });
  });
}

  ngOnInit() {
    const self = this;
    this.route.params.subscribe(params => {
      this.service.editUser(params['id']).then(function(res) {
        self.user = res[0];
        self.createForm();
        debugger;
      });
    });
  }
}
