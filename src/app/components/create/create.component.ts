import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, Inject, ViewChildren } from '@angular/core';
import { UserService } from '../../user.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms'; 

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  title = 'Add User';
  angForm: FormGroup;
  @Input() public users: any;
   @Input() public registerUser: boolean;
  constructor(
    private userservice: UserService,
     private fb: FormBuilder,
     public router: Router
     
   ) {
    
    debugger;
    this.createForm();
   }
  createForm() {
    this.angForm = this.fb.group({
      first_name: ['', Validators.required ],
      last_name: ['', Validators.required ],
      iban: ['', Validators.required ]
   });
  }
  addUser(name, lastname, IBAN) {
    const self = this;
    const obj = {
      first_name: name,
      last_name: lastname,
      iban: IBAN
    };
    this.userservice.addUser(name, lastname, IBAN).then(function() {
      self.router.navigate(['index']);
    });

  }
  ngOnInit() {
    debugger;
  }
}
