import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
  
  id:number;
  result: any; 
  constructor(private http: HttpClient) {}

  addUser(name, lastname, IBAN) {
    const uri = 'http://localhost:4000/users/add';
    const obj = {
      first_name: name,
      last_name: lastname,
      iban: IBAN
    };
    const self = this;
    return this
      .http
      .post(uri, obj).toPromise();

  }

  getUsers(token) {
    const uri = 'https://angular-testcase.herokuapp.com/api/users';
    const body = '';
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' + token
      })
    };

    return this
            .http
            .get(uri, httpOptions)
            .map(res => {
              return res;
            }).toPromise();
  }

  editUser(id) {
    const uri = 'https://angular-testcase.herokuapp.com/api/users';
    const body = '';
    const token = JSON.parse(localStorage.getItem('registerUser')).idToken;
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' + token
      })
    };
    this.id = id;
    var self = this;
    return this
            .http
            .get(uri, httpOptions)
            .map(function(response: Array<any> ) {
           
              return response.filter( user => user.id == self.id );
              
            }).toPromise();
  }

  updateUser(name, lastname, IBAN, id) {
    const uri = 'http://localhost:4000/users/update/' + id;

    const obj = {
      name: name,
      lastname: lastname,
      IBAN: IBAN
    };
    return this
      .http
      .post(uri, obj)
      .toPromise();
  }

  deleteUser(id) {
    const uri = 'http://localhost:4000/users/delete/' + id;

        return this
            .http
            .get(uri)
            .map(res => {
              return res;
            }).toPromise();
  }

}
