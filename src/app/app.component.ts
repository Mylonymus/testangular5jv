import { Component, Input, Output, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthServiceConfig } from 'angularx-social-login';

declare var googleyolo: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
 
export class AppComponent implements OnInit {
  title = 'Welcome to crypto world';

   registerUser = false;
 
  constructor(private router: Router, private ruta: ActivatedRoute, private auth: AuthServiceConfig) {

    this.router.navigate(['login']);
  }

  ngOnInit() { 
    if (localStorage.getItem('registerUser')) {
      this.router.navigate(['index']);
      this.registerUser = true;
    } else {
      this.router.navigate(['login']);
      this.registerUser = false;
      this.loadLogin();
    }
  }

  loadLogin() {
    const self = this;
    const hintPromise = googleyolo.hint({
       supportedAuthMethods: [
          'https://accounts.google.com'
       ],
       supportedIdTokenProviders: [
       {
          uri: 'https://accounts.google.com',
          clientId: '396519560792-flahmhsg3n40gk0nnil821q7ma7rkca8.apps.googleusercontent.com'
       }
      ]
    });

    hintPromise.then(function(res) {

      localStorage.setItem('registerUser', JSON.stringify(res));
      self.router.navigate(['index']);
    }, function(err) {
      debugger;
    });
  }


}

